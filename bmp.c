//
// Created by notnaturalselection on 04.12.2020.
//

#include <stdint.h>
#include <stdlib.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct bit_map_file_header {
    uint16_t bf_type;
    uint32_t bf_size;
    uint16_t bf_reserved1;
    uint16_t bf_reserved2;
    uint32_t bf_off_bits;
};

struct bit_map_info_core {
    uint32_t bc_size;
    uint16_t bc_width;
    uint16_t bc_height;
    uint16_t bc_planes;
    uint16_t bc_bit_count;
};

struct cie_xyz_triple {
    uint32_t red_x;
    uint32_t red_y;
    uint32_t red_z;
    uint32_t green_x;
    uint32_t green_y;
    uint32_t green_z;
    uint32_t blue_x;
    uint32_t blue_y;
    uint32_t blue_z;
};

struct bit_map_info_345 {
    uint32_t bi_size;
    uint32_t bi_width;
    uint32_t bi_height;
    uint16_t bi_planes;
    uint16_t bi_bit_count;
    uint32_t bi_compression;
    uint32_t bi_size_image;
    uint32_t bi_xpels_per_meter;
    uint32_t bi_ypels_per_meter;
    uint32_t bi_clr_used;
    uint32_t bi_clr_important;

    uint32_t b_v4_red_mask;
    uint32_t b_v4_green_mask;
    uint32_t b_v4_blue_mask;
    uint32_t b_v4_alpha_mask;
    uint32_t b_v4_cs_type;
    struct cie_xyz_triple b_v4_endpoints;
    uint32_t b_v4_gamma_red;
    uint32_t b_v4_gamma_green;
    uint32_t b_v4_gamma_blue;;

    uint32_t b_v5_intent;
    uint32_t b_v5_profile_data;
    uint32_t b_v5_profile_size;
    uint32_t b_v5_reserved;
};

struct bmp_info {
    struct bit_map_file_header *bmfh;
    struct bit_map_info_3_4_5 *bmi;
    struct image *img;
};

struct bmp_info *new_bmp() {
    struct bmp_info *bmp =  malloc(sizeof(struct bmp_info));
    bmp->img = malloc(sizeof(struct image));
    return bmp;
}

struct image *rotate(struct image *source) {
    struct image *result = malloc(sizeof(struct image));
    result->width = source->height;
    result->height = source->width;
    result->data = malloc(sizeof(struct pixel) * result->width * result->height);
    for (int i = 0; i < source->width; ++i) {
        for (int j = 0; j < source->height; ++j) {
            result->data[(i + 1) * source->height - (j + 1)] = source->data[j * source->width + i];
        }
    }
    return result;
}