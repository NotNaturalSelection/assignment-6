#include <stdio.h>
#include <stdint.h>

#include "bmp.h"
#include "readbmp.h"
#include "writebmp.h"

int main() {
    struct bmp_info *bmp = new_bmp();
    char filename[50];
    gets(filename);
    char new_filename[15];
    FILE *source = fopen(filename, "r");
    sprintf(new_filename, "(rotated).bmp");
    FILE *dest = fopen(new_filename, "w");
    from_bmp(source, bmp);
    bmp->img = rotate(bmp->img);
    to_bmp(dest, bmp);
}

