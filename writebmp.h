//
// Created by notnaturalselection on 04.12.2020.
//

#ifndef UNTITLED_WRITEBMP_H
#define UNTITLED_WRITEBMP_H
#include "bmp.h"

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

void write_bit_map_file_header(FILE *out, struct bit_map_file_header *bmfh);

void write_bit_map_info_core(FILE *out, struct bit_map_info_core *bmic);

void write_bit_map_info_345(FILE *out, struct bit_map_info_345 *bmi);

void write_pixels(FILE *out, struct image *img_to_write);

enum write_status to_bmp(FILE *out, struct bmp_info *bmp);

#endif //UNTITLED_WRITEBMP_H
